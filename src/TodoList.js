import React, { Component, Fragment } from 'react'
import './style.css'
import TodoItem from './TodoItem'
class TodoList extends Component{

	constructor(props){
		super(props);
		this.state = {
			inputValue:'',
			list: ['學習英文', '學習react']
		}
	}

	render(){
		return(
			<Fragment>
				<div>
					<label htmlFor = "insert_area">輸入內容 </label>
					<input
						id = "insert_area"
						className='input'
						value={this.state.inputValue}
						onChange={this.handleInputChange.bind(this)}
					/>
					<button onClick={this.handleBtnClick.bind(this)} >
						ENTER
					</ button>
				</div>
				<ul>
					{
						this.state.list.map(
							(item,index) => {
							return(
								<div>
								<TodoItem
								content={item}
								index={index}
								deleItem={this.handleItemDel.bind(this)}
								/>

					  			</div>
							)
						})
					}
				</ul>
			</Fragment>
		)
	}


	handleItemDel(index){
		const list = [...this.state.list];
		list.splice(index,1);//刪除
		this.setState({
			list:list
		})
	}
	handleInputChange(e){
		this.setState({
			inputValue: e.target.value
		})
	}
	handleBtnClick(){
		this.setState({
			list: [...this.state.list, this.state.inputValue]
			,inputValue: ''
		})
	}

}


export default TodoList;